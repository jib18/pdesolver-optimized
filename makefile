CC     = gcc -std=c11 -O3 -mavx2 -march=native
CFLAGS = -Wall -Wextra -DLIKWID_PERFMON -I /home/soft/likwid/include
LFLAGS = -L /home/soft/likwid/lib -llikwid -lm

PROG = pdeSolver
OBJS = Funcoes.o utils.o $(PROG).o

all: pdeSolver

pdeSolver:

$(PROG):  $(OBJS)
	$(CC) -o $@ $^ $(LFLAGS)

doc:
	doxygen -g docs
	doxygen docs

clean:
	$(RM) -rf *.o $(PROG) html
