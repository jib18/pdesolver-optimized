/*
*   file pdeSolver.c
*   brief programa main
*   data 07/10/2019   
*      
*   Autores:
*   GRR20182962 John Israel Boldt
*   GRR20182965 João Victor Kenji Tamaki
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "Funcoes.h"

/*
* programa main para resolver uma equação diferencial utilizando Gauss Seidel
*/

int main(int argc, char * argv[]){
    
    int Nx = 0, Ny = 0, maxIter = 0;
    FILE *arqout = stdout;
    double *tempo_medio = (double *) malloc (sizeof(double));
    *tempo_medio = 0.0;
    int erro = 0;

/*
* Verificação de erros de entrada
*/
    for(int i=1; i < argc && !erro; i++){
    if(strcmp(argv[i],"-nx")==0){
	    i++;
	    Nx = atoi(argv[i]);
    }else if(strcmp(argv[i],"-ny")==0) {
	    i++;
	    Ny = atoi(argv[i]);
    }else if(strcmp(argv[i],"-i")==0){
	    i++;
	    maxIter = atoi(argv[i]);
    }else if(strcmp(argv[i],"-o")==0){
	    i++;
      arqout = fopen(argv[i++],"w");
      if(!arqout){ erros(FOPEN); return(0); }
    }else{
      erros(INPUT);
    }
  }
  real_t * normaL2 = (real_t *) malloc (maxIter * sizeof(real_t));

/*
* Discretização da matriz
*/
    real_t hx = PI/(Nx + 1);
    real_t hy = PI/(Ny + 1);
    Nx += 2;
    Ny += 2;
    int tam = Nx * Ny;
    SistLinear_t * SL = alocaSistLinear(Nx, Ny, tam);
    if(!SL) return(erros(MALLOC));

/*
* Método de gauss-seidel
*/
    erro = gaussSeidel(SL, normaL2, maxIter, hx, hy, Nx, Ny, tam, tempo_medio);
    if(erro == 1) return(0);
/*
* Impressão dos resultados
*/
    //imprimeResultado (SL, normaL2, maxIter, tempo_medio, Nx, Ny, arqout);
    
    fclose(arqout);    
    return(0);
}