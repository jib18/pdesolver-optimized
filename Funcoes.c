/*
*   file Funcoes.c
*   Arquivo contendo a implementação das funções utilizadas no arquivo pdeSolver.c
*   data 07/10/2019   
*      
*   Autores:
*   GRR20182962 John Israel Boldt
*   GRR20182965 João Victor Kenji Tamaki
*/

#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"
#include "Funcoes.h"

#include "Funcoes.h"
#ifdef LIKWID_PERFMON
#include <likwid.h>
#else
#define LIKWID_MARKER_INIT
#define LIKWID_MARKER_START(regionTag)
#define LIKWID_MARKER_STOP(regionTag)
#define LIKWID_MARKER_CLOSE
#endif

/*!
* Funçao dada na especificação do trabalho para calcular o tempo de execução em milisegundos
*/

int erros(int erro){

  switch(erro){

    case INPUT: 
        fprintf(stderr, "Erro na entrada!\n"); 
        break;
    case MALLOC: 
        fprintf(stderr, "Não foi possível alocar a memória!\n");
        break;
    case INFINITO: 
        fprintf(stderr, "Alguma operação resultou em infinito!\n");
        break;
    case FOPEN: 
        fprintf(stderr, "Não foi possível abrir o arquivo de saída!\n");
        break;
    default: 
        fprintf(stderr, "Infelizmente ocorreu um erro inesperado!\n");
        break;
  }
  return(0);
}


/*!
*   Função para liberar a estrutura de dados
*/
void liberaSistLinear(SistLinear_t * SL){

  free(SL->U);
  free(SL->b);
  free(SL->r1);
  free(SL->r2);
  free(SL);
}

/*!
* Função para alocar a estrutura de dados
*/
SistLinear_t * alocaSistLinear(int Nx, int Ny, int tam){

  int tam_b = (Nx-2) * (Ny-2);
  SistLinear_t * SL = (SistLinear_t *) malloc(sizeof(SistLinear_t));
  if( SL ){

    SL->U = (real_t *) malloc (tam * sizeof(real_t));
    SL->b = (real_t *) malloc (tam_b * sizeof(real_t));
    SL->r1 = (real_t *) malloc (tam * sizeof(real_t));
    SL->r2 = (real_t *) malloc (tam * sizeof(real_t));

    if( !(SL->U) || !(SL->r1) || !(SL->r2)){
      	liberaSistLinear(SL);
  	  	erros(MALLOC); 
  	}else {
  		memset(SL->U, 0.0, tam*sizeof(real_t));
      memset(SL->b, 0.0, tam_b*sizeof(real_t));
  		memset(SL->r1, 0.0, tam*sizeof(real_t));
      memset(SL->r2, 0.0, tam*sizeof(real_t));
  	}
  }
  return(SL);
}

real_t calcula_norma(real_t * residuos, int N){

  real_t sum = 0.0;
  for(int i = 0; i < N; i++){
    sum += residuos[i] * residuos[i];
  }
  return (sum);

}

int gaussSeidel (SistLinear_t *SL, real_t * normaL2, int maxIter, real_t hx, real_t hy, int Nx, int Ny, int tam, double *tempo_medio){

  LIKWID_MARKER_INIT;
  LIKWID_MARKER_START("GaussSeidel");   

  double tempo_inicial = timestamp();
  double tempo_final = 0.0;
  double tempo_Iter = 0.0;
  int i1, i2, j1, j2;
  int numIter = 0;
  real_t d1, d2;

  //OPERAÇÕES ECONOMIZADAS
  real_t p2_hx = hx * hx;
  real_t p2_hy = hy * hy;
  real_t p2_PI = PI * PI;
  real_t m2PI = 2 * PI;
  real_t sinh_p2_PI = sinh(p2_PI);
  real_t multiplicador_f = (8 * p2_hx) * (p2_hy * p2_PI);

  coeficientes coef;
  coef.c = 4 * p2_hx + 4 * p2_hy + 8 * p2_hx * p2_hy * p2_PI; //central
  coef.d = -2 * p2_hy - p2_hy * hx; //direita
  coef.e = -2 * p2_hy + p2_hy * hx; //esquerda
  coef.i = -2 * p2_hx + p2_hx * hy; //inferior
  coef.s = -2 * p2_hx - p2_hx * hy; //superior

  //CALCULO DOS VALORES DE SENO DEPENDENTES DO ÍNDICE J UTILIZADOS NO MÉTODO
  op_trigonometrico *intervalo_trigonometrico_j = (op_trigonometrico *) malloc ((Ny-1) *sizeof(op_trigonometrico));
  for(int j = 1; j < Ny - 1; j++){
        
    real_t op_seno1 = m2PI * hx * j;
    real_t op_seno2 = m2PI * (PI- hx * j);
    intervalo_trigonometrico_j[j-1].operadores[1] = sin(op_seno1);
    intervalo_trigonometrico_j[j-1].operadores[2] = sin(op_seno2);
  }

  //CALCULO DOS VALORES DE SENOH DEPENDENTES DO ÍNDICE I UTILIZADOS NO MÉTODO
  op_trigonometrico *intervalo_trigonometrico_i = (op_trigonometrico *) malloc ((Nx-1) *sizeof(op_trigonometrico));
  for(int i = 1; i < Nx - 1; i++){

    real_t op_senoh1 = PI * i * hy;
    real_t op_senoh2 = PI * (PI - i*hy);
    intervalo_trigonometrico_i[i-1].operadores[1] = sinh(op_senoh1);
    intervalo_trigonometrico_i[i-1].operadores[2] = sinh(op_senoh2);
  }

  //CALCULO DOS VALORES DE B
  int Ny_b = Ny-2;
  int Nx_b = Nx-2;

  for(int i = 0; i < Nx_b; i++){

    int i_Ny_b = i*Ny_b;
    for(int j = 0; j < Ny_b; j++){

      SL->b[i_Ny_b + j]  = multiplicador_f * (intervalo_trigonometrico_j[j-1].operadores[1] * intervalo_trigonometrico_i[i-1].operadores[1]
                                            + intervalo_trigonometrico_j[j-1].operadores[2] * intervalo_trigonometrico_i[i-1].operadores[2]);
      //OPERAÇÃO REAL: f = 8 * hx^2 * hy^2 * PI^2 * (sin(2*PI*hx*j) * sinh(PI*i*hy) + sin(2*PI * (PI - j*hx)) * sinh(PI * (PI - i*hy)))
    }

  } 

  //----------------------------------------------------------------------------

  /*!
  * Método de Gauss-Seidel
  */

    /*!
    *cálculo das bordas
    */

  //LOOP FUSION DAS OPERAÇÕES DE BORDA
  for(int j = 1; j < Ny - 1; j++){
  
    SL->U[(Nx-1) * Ny+j] = intervalo_trigonometrico_j[j-1].operadores[1] * sinh_p2_PI;
    SL->U[j] = intervalo_trigonometrico_j[j-1].operadores[2] * sinh_p2_PI;
        
    //SL->U[(Nx - 1) * Ny + j] = sin(m2PI * j * hx) * sinh_p2_PI;
    //SL->U[j] = sin(m2PI * (PI- hx*j)) * sinh_p2_PI;
  }
  //------------------------------------------------------------

  tempo_final = timestamp();
  *tempo_medio = tempo_final - tempo_inicial;

  while(numIter < maxIter - 1){

    tempo_inicial = timestamp();
    
    //cálculo das primeiras duas linhas
    for(i1 = 1; i1 < 3; i1++){
      for(j1 = 1;j1 < Ny - 1; j1++){

        d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1*Ny+(j1-1)]) + (coef.i*SL->U[i1*Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
        SL->U[i1 * Ny + j1] = (SL->b[(i1-1)*Ny_b + j1] - d1)/coef.c;

        /*!
        * cálculo do resíduo
        */
        if(i1 != 1){
          i1--;
          d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1 * Ny+(j1-1)]) + (coef.i*SL->U[i1 * Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
          SL->r1[(i1-1) * Nx + j1] = SL->b[(i1-1)*Ny_b + j1] - (d1 + coef.c * SL->U[i1 * Ny + j1]);
          i1++;
        }        
      }
    }

    for(i1 = 3, i2 = 1; i1 < Nx - 1; i1++, i2 ++){

      //CALCULO DO PRIMEIRO ITEM DA LINHA - ITERACAO 1
      j1 = 1;
      d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1*Ny+(j1-1)]) + (coef.i*SL->U[i1*Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
      SL->U[i1*Ny + j1] = (SL->b[(i1-1)*(Ny-2) + j1] - d1)/coef.c;
      
      //RESIDUO
      i1--;
      d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1 * Ny+(j1-1)]) + (coef.i*SL->U[i1 * Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
      SL->r1[(i1-1) * Nx + j1] = (SL->b[(i1-1)*(Ny-2) + j1] - (d1 + coef.c * SL->U[i1 * Ny + j1]));
      i1++;

      for(j1 = 2, j2 = 1; j1 < Ny - 1; j1++, j2++){

        d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1*Ny+(j1-1)]) + (coef.i*SL->U[i1*Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
        d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2*Ny+(j2-1)]) + (coef.i*SL->U[i2*Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
        SL->U[i1*Ny + j1] = (SL->b[(i1-1)*Ny_b + j1] - d1)/coef.c;
        SL->U[i2*Ny + j2] = (SL->b[(i2-1)*Ny_b + j2] - d2)/coef.c;

        /*!
        * cálculo do resíduo
        */
        i1--;
        d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1 * Ny+(j1-1)]) + (coef.i*SL->U[i1 * Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
        SL->r1[(i1-1) * Nx + j1] = SL->b[(i1-1)*Ny_b + j1] - (d1 + coef.c * SL->U[i1 * Ny + j1]);
        i1++;

        if(i2!=1){
          i2--;
          d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2 * Ny+(j2-1)]) + (coef.i*SL->U[i2 * Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
          SL->r2[(i2-1) * Nx + j2] = SL->b[(i2-1)*Ny_b + j2] - (d2 + coef.c * SL->U[i2 * Ny + j2]);
          i2++;
        }

        if(SL->U[i1 * Nx + j1] == INFINITY || SL->U[i2 * Nx + j2] == INFINITY){ erros(INFINITO); return(1); }

        //CALCULO DO ÚLTIMO ITEM DA LINHA - ITERACAO 2
        j2 = Ny-2;
        d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2*Ny+(j2-1)]) + (coef.i*SL->U[i2*Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
        SL->U[i2*Ny + j2] = (SL->b[(i2-1)*Ny_b + j2] - d2)/coef.c;
      
        //RESIDUO
        i2--;
        d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2 * Ny+(j2-1)]) + (coef.i*SL->U[i2 * Ny+(j2+1)] + coef.e*SL->U[(i2+1) * Ny+j2]);
        SL->r2[(i2-1) * Nx + j2] = SL->b[(i2-1)*Ny_b + j2] - (d2 + coef.c * SL->U[i2 * Ny + j2]);
        i2++;

      }
    }

    //RESÍDUO DA ÚLTIMA LINHA ITERAÇÃO 1
    i1 = Nx-2;

    int indice_residuo = (i1-1)*Nx;

    for(j1 = 1; j1 < Ny-1; j1 ++){
      
      d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1 * Ny+(j1-1)]) + (coef.i*SL->U[i1 * Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
      SL->r1[indice_residuo + j1] = SL->b[(i1-1)*Ny_b + j1] - (d1 + coef.c * SL->U[i1 * Ny + j1]);
    }

    //CALCULO DAS ÚLTIMAS DUAS LINHAS DA ITERAÇÃO 2
    for(j2 = Ny-3; j2 < Ny-1; j2++){

      d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2*Ny+(j2-1)]) + (coef.i*SL->U[i2*Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
      SL->U[i2*Ny + j2] = (SL->b[(i2-1)*Ny_b + j2] - d2)/coef.c;

      /*!
      * cálculo do resíduo
      */
      i2--;
      d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2 * Ny+(j2-1)]) + (coef.i*SL->U[i2 * Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
      SL->r2[(i2-1) * Nx + j2] = SL->b[(i2-1)*Ny_b + j2] - (d2 + coef.c * SL->U[i2 * Ny + j2]);
      i2++;
    }

    //RESÍDUO DA ÚLTIMA LINHA ITERAÇÃO 2
    i2 = Nx-2;

    //OPERAÇÕES ECONOMIZADAS
    indice_residuo = (i2-1)*Nx;
    //------------------------------

    for(j2 = 1; j2 < Ny-1; j2 ++){
      
      d2 = (coef.d*SL->U[(i2-1)*Ny+j2] + coef.s*SL->U[i2 * Ny+(j2-1)]) + (coef.i*SL->U[i2 * Ny+(j2+1)] + coef.e*SL->U[(i2+1)*Ny+j2]);
      SL->r2[indice_residuo + j2] = SL->b[(i2-1)*Ny_b + j2] - (d2 + coef.c * SL->U[i2 * Ny + j2]);
    }
      
    /*!
    * cálculo da norma L2
    */
    normaL2[numIter] = calcula_norma(SL->r1, tam);
    numIter++;

    normaL2[numIter] = calcula_norma(SL->r2, tam);
    numIter++;
    
    tempo_final = timestamp();
    tempo_Iter = tempo_final - tempo_inicial;
    *tempo_medio += tempo_Iter;

  } //fim do while

  tempo_inicial = timestamp();
  if(numIter == maxIter-1){
    for(i1 = 1; i1 < Nx - 1; i1++){

      for(j1 = 1; j1 < Ny - 1; j1++){

        d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1*Ny+(j1-1)]) + (coef.i*SL->U[i1*Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
        SL->U[i1*Ny + j1] = (SL->b[(i1-1)*Ny_b + j1] - d1)/coef.c;

        /*!
        * cálculo do resíduo
        */

        if(i1 != 1){
          i1--;
          d1 = (coef.d*SL->U[(i1-1)*Ny+j1] + coef.s*SL->U[i1 * Ny+(j1-1)]) + (coef.i*SL->U[i1 * Ny+(j1+1)] + coef.e*SL->U[(i1+1)*Ny+j1]);
          SL->r1[(i1-1) * Nx + j1] = SL->b[(i1-1)*Ny_b + j1] - (d1 + coef.c * SL->U[i1 * Ny + j1]);
          i1++;
        }
      }
    }
    numIter ++;
    normaL2[numIter]= calcula_norma(SL->r1, tam);
    tempo_final = timestamp();
    tempo_Iter = tempo_final - tempo_inicial;
    *tempo_medio += tempo_Iter;
  }
  
    
  /*!
  * O tempo é calculado a partir do início da iteração do método até a obtenção do vetor solução daquela iteração.
  * O resultado deve ser a média aritmética do tempo de todas iterações.
  */
  
  *tempo_medio /= maxIter;
  printf(" #Tempo Método GS: %4f ms\n", *tempo_medio);
  LIKWID_MARKER_STOP("GaussSeidel");
  LIKWID_MARKER_CLOSE;
  return(0);
}

/* void imprimeResultado (SistLinear_t *SL, real_t *normaL2, int maxIter, double *tempo_medio, int Nx, int Ny, FILE *arqout){
    
  
  * Impressão no formato de comentário do Gnuplot

  fprintf(arqout, " ###########\n");
  fprintf(arqout, " #Tempo Método GS: %4f ms\n", *tempo_medio); 
  
  for (int i = 0; i < maxIter; i++){
      fprintf(arqout, " #i=%d: %10g\n", i + 1, sqrt(normaL2[i]));
  }
  fprintf(arqout, " ###########\n # \t X    \t Y    \t Z\n");
   
  for(int i = 1; i < Nx-1; i++){
    for(int j = 1; j < Ny-1; j++){ 
       
      fprintf(arqout, "%e %e %e\n", i*PI/Nx, j*PI/Ny, SL->U[i*Ny + j]);
    }
  }

} */
